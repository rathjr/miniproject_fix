package SRG3;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Scanner;

public class Stock implements Serializable {
    private int id,qty;
    private double unitprice;
    private String name;
    public Stock(int id, String name, double unitprice, int qty){
        this.id=id;
        this.name=name;
        this.unitprice=unitprice;
        this.qty=qty;
    }
    public Stock(){}
    public static Scanner cin = new Scanner(System.in);
    public void AddnewStock(int autoid) {
        int count=0;
        id=autoid;
                System.out.println("=> Product ID      :  "+id);
        do {
            System.out.print("=> Product's Name    :  ");
            name = cin.nextLine();
            if (Regex.onlystring(name))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(name));
        do {
            try {
                System.out.print("=> Product's Price  :  ");
                unitprice = cin.nextDouble();
                count = 0;
                if (unitprice < 0) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        do {
            try {
                System.out.print("=> Product's Qty      :  ");
                qty = cin.nextInt();
                count = 0;
                if (qty < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
    }

    public void Updatename(){
        do {
            System.out.print("=> Enter Product Name    :  ");
            name = cin.nextLine();
            if (Regex.onlystring(name))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(name));
    }
    public void Updateqty(){
        int count=0;
        do {
            try {
                System.out.print("=> Enter QTY      :  ");
                qty = cin.nextInt();
                count = 0;
                if (qty < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
    }
    public void Updateunitprice(){
        int count=0;
        do {
            try {
                System.out.print("=> Enter UnitPrice  :  ");
                unitprice = cin.nextDouble();
                count = 0;
                if (unitprice < 0) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
    }
    public void UpdateAll(){
        int count=0;
        do {
            System.out.print("=> Enter Product Name    :  ");
            name = cin.nextLine();
            if (Regex.onlystring(name))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(name));
        do {
            try {
                System.out.print("=> Enter UnitPrice  :  ");
                unitprice = cin.nextDouble();
                count = 0;
                if (unitprice < 0) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        do {
            try {
                System.out.print("=> Enter QTY      :  ");
                qty = cin.nextInt();
                count = 0;
                if (qty < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
    }
    public int getId() {
        return id;
    }

    public int getQty() {
        return qty;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public String getName() {
        return name;
    }

    public LocalDate getMyObj() {
        return myObj;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMyObj(LocalDate myObj) {
        this.myObj = myObj;
    }

    LocalDate myObj = LocalDate.now();

    public String Tostring(){
        return "\n"+"id : "+getId()+"\n"+"Name : "+getName()+"\n"+"Unitprice : "+getUnitprice()+"\n"+"Qty : "+getQty()+"\n"+"Import Date : "+myObj+"\n";
    }

    @Override
    public String toString() {
        return "\n"+id+"\t"+getName()+"\t"+getUnitprice()+"\t"+getQty()+"\t"+myObj;
    }
}
