package SRG3.BaseFeature;

import SRG3.Regex;
import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.List;

import static SRG3.Stock.cin;

public class Search {
    public static void Search(List<Stock> object){
        String search;
        int count=0;
        do {
            System.out.print("=> Enter Name to Search    :  ");
            search = cin.nextLine();
            if (Regex.onlystring(search))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(search));
        CharSequence seq = search;
        boolean bool;
        for(Stock st: object){
            bool=st.getName().contains(seq);
            if(search.equals(st.getName())|| bool==true){
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);

                t1.addCell(" ID :"+st.getId(),numberStyle1);
                t1.addCell( " Name :"+st.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+st.getQty(),numberStyle1);
                t1.addCell(" Date :"+st.getMyObj(),numberStyle1);

                System.out.println(t1.render());
                count++;
            }
        }
        if(count==0){
            System.out.println("=> search not found");
        }

    }
}
