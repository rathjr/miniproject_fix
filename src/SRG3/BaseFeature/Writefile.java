package SRG3.BaseFeature;

import SRG3.Stock;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class Writefile {
    public static void Writefile(File file, List<Stock> object){
        try(ObjectOutputStream obs=new ObjectOutputStream(new FileOutputStream(file))){
            obs.writeObject(object);
            obs.flush();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
//        try(ObjectOutputStream obs=new ObjectOutputStream(new FileOutputStream(file));){
//            obs.writeObject(object);
//            obs.flush();
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
    }
}
