package SRG3.BaseFeature;

import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.Iterator;
import java.util.List;

import static SRG3.Stock.cin;

public  class Delete {
    public static void RemoveList(List<Stock> object) {
        int id = 0, count = 0;
        char veri;
        do {
            try {
                System.out.print("=> Enter ID Of Product       :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        Iterator<Stock> it = object.iterator();
        while (it.hasNext()) {
            Stock tmp = it.next();
            if (id == tmp.getId()) {
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);

                t1.addCell(" ID :"+tmp.getId(),numberStyle1);
                t1.addCell( " Name :"+tmp.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+tmp.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+tmp.getQty(),numberStyle1);
                t1.addCell(" Date :"+tmp.getMyObj(),numberStyle1);

                System.out.println(t1.render());
                System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                veri=cin.next().charAt(0);
                while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                    System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                    veri=cin.next().charAt(0);
                }
                if (veri == 'y' || veri == 'Y') {
                    it.remove();
                    System.out.println("=> Remove Successfully !");
                } else {
                    System.out.println("Your choice is Cancel !");
                }
                count++;
            }
        }
        if (count == 0)
            System.out.println("=> ID : " + id + " IS NOT EXIST...");
    }
}
