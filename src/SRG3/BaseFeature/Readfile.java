package SRG3.BaseFeature;

import SRG3.Stock;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class Readfile {
    public static void Readfile(File file, List<Stock> object){
//        try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(file));){
//            ArrayList stock = (ArrayList<Stock>) ois.readObject();
//            ois.close();
//            System.out.println(stock.toString());
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
        try(ObjectInputStream ois=new ObjectInputStream(new FileInputStream(file))){
            List<Stock> read  = (ArrayList<Stock>) ois.readObject();
            object.addAll(read);
        }catch (Exception e){
            e.getMessage();
        }
    }
}
