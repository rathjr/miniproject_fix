package SRG3.BaseFeature;

public class Loading extends Thread
{
    public String text;
    public int delayTime;
    boolean isLoopCount;

    public Loading(String text, int delayTime, boolean isLoopCount) {
        this.text = text;
        this.delayTime = delayTime;
        this.isLoopCount = isLoopCount;
    }
    static void waiting(){
        String name="Please Wait Loading.....";
        for (int i = 0; i < name.length(); i++) {
            System.out.print(name.charAt(i));
            try {
                sleep(50);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }
    @Override
    public void run()
    {
        loadingPrint(this.text, this.delayTime,this.isLoopCount);
    }

    void loadingPrint(String text, int delayTime, boolean countLoad){
        if (isLoopCount == false) {
            System.out.print(text);
            try {
                Thread.sleep(delayTime);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else{
            for (int i=0; i<text.length(); i++)
            {
                System.out.print(text.charAt(i));
                try{
                    Thread.sleep(delayTime);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
            System.out.println();
        }
    }
}
