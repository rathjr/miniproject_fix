package SRG3.BaseFeature;

import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.List;
import java.util.Scanner;

public class ReadbyID {
    public static void ReadbyID(List<Stock> object) {
        Scanner cin=new Scanner(System.in);
        int id=0,count=0;
        do {
            try {
                System.out.print("=> Enter ID Of Product      :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        for(Stock st:object){
            if(id==st.getId()){
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);

                t1.addCell(" ID :"+st.getId(),numberStyle1);
                t1.addCell( " Name :"+st.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+st.getQty(),numberStyle1);
                t1.addCell(" Date :"+st.getMyObj(),numberStyle1);

                System.out.println(t1.render());
                count++;
            }
        }
        if(count==0){
            System.out.println("-> Your ID id EXIST....");
        }
    }
}
