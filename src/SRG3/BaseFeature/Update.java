package SRG3.BaseFeature;

import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.List;

import static SRG3.Stock.cin;

public class Update {
    public static void Update(List<Stock> object){
        int id=0,count=0,ch=0,ct=0;
        do {
            try {
                System.out.print("=> Enter ID Of Product       :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        for(Stock st:object) {
            if (id == st.getId()) {
                CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                t1.setColumnWidth( 0,32,32);

                t1.addCell(" ID :"+st.getId(),numberStyle1);
                t1.addCell( " Name :"+st.getName(), numberStyle1);
                t1.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle1);
                t1.addCell(" Qty :"+st.getQty(),numberStyle1);
                t1.addCell(" Date :"+st.getMyObj(),numberStyle1);

                System.out.println(t1.render());

                ct++;

                // System.out.println("1. All"+"\t"+"\t"+"2. Name"+"\t"+"\t"+"3. Qty"+"\t"+"\t"+"4. Unitprice"+"\t"+"\t"+"5. Back to menu");
                System.out.println("What do you want to update ?");
                System.out.println("╔════════════════════════════════════════════════════════════════════════════╗");
                System.out.println("║ 1. All     2. Name     3.Quantity      4.Unit Price        5.Back to Menu  ║");
                System.out.println("╚════════════════════════════════════════════════════════════════════════════╝");

                do {
                    try {
                        System.out.print("=> Choose Option : ");
                        ch = cin.nextInt();
                        count = 0;
                        if (id < 0)
                            throw new Exception();
                    } catch (Exception e) {
                        System.out.println("-> INPUT IS INVALID ! (Please Input Follow Menu)....");
                        count++;
                    }
                    cin.nextLine();
                } while (count != 0);
                switch (ch) {
                    case 1:
                        st.UpdateAll();
                        char veri;
                        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t.setColumnWidth( 0,32,32);

                        t.addCell(" ID :"+st.getId(),numberStyle);
                        t.addCell( " Name :"+st.getName(), numberStyle);
                        t.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle);
                        t.addCell(" Qty :"+st.getQty(),numberStyle);
                        t.addCell(" Date :"+st.getMyObj(),numberStyle);

                        System.out.println(t.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                        break;
                    case 2:
                        st.Updatename();
                        CellStyle numberStyle2 = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t2 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t2.setColumnWidth( 0,32,32);

                        t2.addCell(" ID :"+st.getId(),numberStyle2);
                        t2.addCell( " Name :"+st.getName(), numberStyle2);
                        t2.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle2);
                        t2.addCell(" Qty :"+st.getQty(),numberStyle2);
                        t2.addCell(" Date :"+st.getMyObj(),numberStyle2);

                        System.out.println(t2.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                        break;
                    case 3:
                        st.Updateqty();
                        CellStyle numberStyle3 = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t3 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t3.setColumnWidth( 0,32,32);

                        t3.addCell(" ID :"+st.getId(),numberStyle3);
                        t3.addCell( " Name :"+st.getName(), numberStyle3);
                        t3.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle3);
                        t3.addCell(" Qty :"+st.getQty(),numberStyle3);
                        t3.addCell(" Date :"+st.getMyObj(),numberStyle3);

                        System.out.println(t3.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                        break;
                    case 4:
                        st.Updateunitprice();
                        CellStyle numberStyle4 = new CellStyle(CellStyle.HorizontalAlign.left);
                        Table t4 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                        t4.setColumnWidth( 0,32,32);

                        t4.addCell(" ID :"+st.getId(),numberStyle4);
                        t4.addCell( " Name :"+st.getName(), numberStyle4);
                        t4.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle4);
                        t4.addCell(" Qty :"+st.getQty(),numberStyle4);
                        t4.addCell(" Date :"+st.getMyObj(),numberStyle4);

                        System.out.println(t4.render());
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            System.out.println("=> Update Successfully !");
                        } else {
                            System.out.println("Your choice is Cancel !");
                        }
                        cin.nextLine();
                        break;
                    case 5:
                        break;
                    default:
                        System.out.println("-> INPUT IS INVALID ! (Please Input Follow Menu)....");

                }
            }
        }
        if(ct==0){
            System.out.println("-> Your ID id EXIST....");
        }
    }
}
