package SRG3.BaseFeature;

import SRG3.Kimhak.*;
import SRG3.Moni.Help;
import SRG3.Moni.TableDisplay;
import SRG3.Regex;
import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import static SRG3.Stock.cin;

public class Merge {

    int ab=0,l=0,cot=1;
    int fileindex=1;
    int autoid=0,recovery=0;
    int a=0;
    String parentpath="C:\\miniproject_fix\\src\\SRG3\\File";
    File file=new File(parentpath,"Trojan1.bak");
    int p =1;
    int page = 1;
    int f = 1;
    int pre=0;
    String idf="Malware";
    List<Stock> object;
    Scanner cin=new Scanner(System.in);
    public Merge(){
        object = new ArrayList<Stock>();
        initializeList();
                Readfile.Readfile(file, object);
    }
    String ar[]=new String[100];
    public void initializeList(){
//        Stock obj1 = new Stock(1, "coca", 30.00, 5);
//        Stock obj2 = new Stock(2, "String", 40.00, 2);
//        Stock obj3 = new Stock(3, "fanta", 50.00, 3);
//        Stock obj4 = new Stock(4, "fanta", 50.00, 3);
//        object.add(obj1);
//        object.add(obj2);
//        object.add(obj3);
//        object.add(obj4);
//       Writefile.Writefile(file,object);
    }
    public void Save(){
        Writefile.Writefile(file,object);
        System.out.println("--> Your Data Was Save Successfully... ");
    }

    public void menu() throws IOException {
        ShowHeader.showHeader();
        do {
            System.out.println("╔══════════════════════════════════════════════════════════════════════════════════════════╗");
            System.out.println("║ *)Display | W)rite | R)ead    | U)pdate | D)elete | F)irst   | P)revious | N)ext | L)ast ║");
            System.out.println("║ S)each    | G)oto  | Se)t row | Sa)ve   | Ba)ckup | Re)store | He)lp     | E)xit         ║");
            System.out.println("╚══════════════════════════════════════════════════════════════════════════════════════════╝");
            String ch;
            autoid=object.size()+1;
            System.out.print("\n"+"Commend-> : ");
            ch = cin.nextLine();
            String[] arrOfStr = ch.split("-");
            char c=ch.charAt(0);

            if(c=='#')
                ch="ws";
            else if(c=='$')
                ch="rs";
            else if(c=='@')
                ch="ds";
            switch (ch) {
                case "*":
                    if(ab==0) {
                            try {
                                show();
                            } catch (Exception e) {
                                System.out.println("-> Your file not found please Copy Path of 'Trojan1'.bak file in the left menu and pass in File of Merge class...."+"\n"+"Good Bye !");
                                System.exit(0);
                            }
                        ab++;
                        autoid=object.size()+1;
                    }else {
                        show();
                    }
                    break;
                case "w":
                case "W":
                    Stock st=new Stock();
                    recovery++;
                    Write.Write(object);
                    char veri;
                    st.AddnewStock(autoid);
                    CellStyle numberStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
                    Table t1 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                    t1.setColumnWidth( 0,32,32);
                    t1.addCell(" ID :"+st.getId(),numberStyle1);
                    t1.addCell( " Name :"+st.getName(), numberStyle1);
                    t1.addCell(" UnitPrice :"+st.getUnitprice(),numberStyle1);
                    t1.addCell(" Qty :"+st.getQty(),numberStyle1);
                    t1.addCell(" Date :"+st.getMyObj(),numberStyle1);

                    System.out.println(t1.render());
                    System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                    veri=cin.next().charAt(0);
                    while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                    }
                    if (veri == 'y' || veri == 'Y') {
                        object.add(st);
                        autoid++;
                        System.out.println("=> Add Successfully !");
                    } else {
                        System.out.println("Your choice is Cancel !");
                    }
                    cin.nextLine();
                    break;
                case "ws":
                    String name=null;
                    int qty=0;
                    double pric=0;
                    try{
                        name=arrOfStr[1];
                    }catch (Exception e){
                        System.out.println("-> YOUR DATA IS INVALID !");
                    }
                    try {
                        pric=Double.parseDouble(arrOfStr[2]);
                    }catch (Exception e){
                        System.out.println("-> YOUR DATA IS INVALID !");
                    }
                    try {
                        qty= Integer.parseInt(arrOfStr[3]);
                    }catch (Exception e){
                        System.out.println("-> YOUR DATA IS INVALID !");
                    }
                Stock in=new Stock(autoid,name,pric,qty);
                    CellStyle numberStyle10 = new CellStyle(CellStyle.HorizontalAlign.left);
                    Table t10 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                    t10.setColumnWidth( 0,32,32);
                    t10.addCell(" ID :"+in.getId(),numberStyle10);
                    t10.addCell( " Name :"+in.getName(), numberStyle10);
                    t10.addCell(" UnitPrice :"+in.getUnitprice(),numberStyle10);
                    t10.addCell(" Qty :"+in.getQty(),numberStyle10);
                    t10.addCell(" Date :"+in.getMyObj(),numberStyle10);

                    System.out.println(t10.render());
                    System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                    veri=cin.next().charAt(0);
                    while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                        System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                    }
                    if (veri == 'y' || veri == 'Y') {
                        object.add(in);
                        autoid++;
                        System.out.println("=> Add Successfully !");
                    } else if(veri=='n'||veri=='N') {
                        System.out.println("Your choice is Cancel !");
                    }
                    cin.nextLine();
                    break;
                case "r":
                case "R":
                    ReadbyID.ReadbyID(object);
                    break;
                case "rs":
                    int id=0,count=0;
                    try{
                        id=Integer.parseInt(arrOfStr[1]);
                    }catch (Exception e){
                        System.out.println("-> YOUR DATA IS INVALID !");
                    }
                    for(Stock sk:object){
                        if(id==sk.getId()){
                            CellStyle numberStyle20 = new CellStyle(CellStyle.HorizontalAlign.left);
                            Table t20 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                            t20.setColumnWidth( 0,32,32);

                            t20.addCell(" ID :"+sk.getId(),numberStyle20);
                            t20.addCell( " Name :"+sk.getName(), numberStyle20);
                            t20.addCell(" UnitPrice :"+sk.getUnitprice(),numberStyle20);
                            t20.addCell(" Qty :"+sk.getQty(),numberStyle20);
                            t20.addCell(" Date :"+sk.getMyObj(),numberStyle20);

                            System.out.println(t20.render());
                            count++;
                        }
                    }
                    if(count==0){
                        System.out.println("-> Your ID id EXIST....");
                    }
                    break;
                case "u":
                case "U":
                    recovery++;
                    Update.Update(object);
                    break;
                case "d":
                case "D":
                    recovery++;
                    Delete.RemoveList(object);
                    break;
                case "ds":
                    int iddel=0,cu=0;
                    try{
                        iddel=Integer.parseInt(arrOfStr[1]);
                    }catch (Exception e){
                        System.out.println("-> YOUR DATA IS INVALID !");
                    }
                    Iterator<Stock> it = object.iterator();
                    while (it.hasNext()) {
                        Stock tmp = it.next();
                        if (iddel == tmp.getId()) {
                            cu++;
                            CellStyle numberStyle30 = new CellStyle(CellStyle.HorizontalAlign.left);
                            Table t30 = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
                            t30.setColumnWidth( 0,32,32);

                            t30.addCell(" ID :"+tmp.getId(),numberStyle30);
                            t30.addCell( " Name :"+tmp.getName(), numberStyle30);
                            t30.addCell(" UnitPrice :"+tmp.getUnitprice(),numberStyle30);
                            t30.addCell(" Qty :"+tmp.getQty(),numberStyle30);
                            t30.addCell(" Date :"+tmp.getMyObj(),numberStyle30);

                            System.out.println(t30.render());
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                            while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                                System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                                veri=cin.next().charAt(0);
                            }
                            if (veri == 'y' || veri == 'Y') {
                                it.remove();
                                System.out.println("=> Remove Successfully !");
                            } else {
                                System.out.println("Your choice is Cancel !");
                            }
                        }
                    }
                    if (cu == 0)
                        System.out.println("=> ID : " + iddel + " IS NOT EXIST...");
                    cin.nextLine();
                    break;
                case "f":
                case "F":
                    FirstPage.FirstPage(object);
                    f = 1;
                    break;
                case "p":
                case "P":
//                    try {
//                        if(pre>=1) {
//                            pre--;
//                            PreviousPage.PreviousPage(object,pre);
//                        }else if(p!=0){
//                            p--;
//                            PreviousPage.PreviousPage(object,p);
//                        }
//                    }catch (Exception e) {
//                        System.out.println("You are already in the fist page!");
//                    }
                    if(p==1){
                        PreviousPage.PreviousPage(object, p);
                    }
                    else if(p >= 1) {
                        p--;
                        PreviousPage.PreviousPage(object, p);
                    }
                    break;
                case "n":
                case "N":
                    System.out.println(f);
                    if(f==1) {
                        NextPage.NextPage(object, f);
                        f++;
                    }else if(p!=0){
                        NextPage.NextPage(object,p);
                        p++;
                    }
                    break;
                case "l":
                case "L":
                    LastPage.LastPage(object);
                    break;
                case "s":
                case "S":
                    Search.Search(object);
                    break;
                case "g":
                case "G":
                    int pa=0,ct=0;
                    do {
                        try {
                            System.out.print("-> Go to page: ");
                            pa = cin.nextInt();
                            ct = 0;
                            if (pa < 0)
                                throw new Exception();
                        } catch (Exception e) {
                            System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                            ct++;
                        }
                        cin.nextLine();
                    } while (ct != 0);
                    p=pa;
                    Goto.Goto(object,pa);
                    break;
                case "se":
                case "Se":
                    SetRow.SetRow(object);
                    break;
                case "sa":
                case "Sa":
                    recovery=0;
                    Save();
                    break;
                case "ba":
                case "Ba":
                     cot++;
//                     String f="Trojan"+fileindex++;
//                     String se=f+".txt";
                    ar[l]=parentpath+"\\"+idf+""+fileindex;
                   backup(ar,l);
                    l++;
                    break;
                case "re":
                case "Re":
                    restore();
                    break;
                case "he":
                case "He":
                    Help.help();
                    break;
                case "1k":
                case "1K":
                    InsertBigdata.Insert10MRecords(object,1000);
                    break;
                case "1m":
                case "1M":
                    InsertBigdata.Insert10MRecords(object,1000000);
                    break;
                case "10m":
                case "10M":
                    InsertBigdata.Insert10MRecords(object,10000000);
                    System.out.println("hello");
                    break;
                case "ex":
                case "Ex":
                    if(recovery!=0){
                        System.out.print("Do you want to save your data ? [Y/y] or [N/n] : ");
                        veri=cin.next().charAt(0);
                        while (!(veri=='y'||veri=='Y'||veri=='n'||veri=='N')) {
                            System.out.print("Are you sure want to add this record ? [Y/y] or [N/n] : ");
                            veri=cin.next().charAt(0);
                        }
                        if (veri == 'y' || veri == 'Y') {
                            Save();
                        } else {
                            System.out.println("Your chose is cancel");
                        }
                    }
                    System.out.println("--good bye-- ):");
                    System.exit(0);
                    break;
                default:
                    System.out.println("-> INPUT IS INVALID ! (Please Input Follow Menu)....");
            }
        }while(true);

    }
    public void table() {
        TableDisplay ta=new TableDisplay();
        ta.ShowList(object);
    }
    public void show(){
        DisplayPagination tap = new DisplayPagination();
        tap.Display(object);
    }

    public void backup(String args[],int l) throws IOException {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        int fileSize = 0, data = 0;

        fis = new FileInputStream(file);
        fileSize = fis.available();
        if (fileSize == 0)
        {
            System.out.println("Sorry, this file is empty.");
            fis.close();
            return;
        }
        fos = new FileOutputStream(args[l] + ".bak");
        while ( (data = fis.read()) != -1)
        {
            fos.write(data);
        }
        fos.flush();

        System.out.println("file "+idf+""+fileindex+" craeted successfully.");
        fileindex++;
        fos.close();
        fis.close();
    }
    public void restore(){
        for(int i=1;i<cot;i++){
            System.out.println(i+". " +idf+i);
        }
        int ch;
        System.out.println("Please choose SRG3.File to Restore : ");
        ch=cin.nextInt();
        System.out.println(ch);


    }


    }

