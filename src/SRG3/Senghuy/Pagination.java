package SRG3.Senghuy;

import SRG3.BaseFeature.Writefile;
import SRG3.Moni.TableDisplay;
import SRG3.Stock;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Pagination {
    File file = new File("D:\\MiniProject\\src\\SR3\\Virus.bak");
    static List<Stock> object;

    public Pagination() {
        object = new ArrayList<Stock>();
        initializeList();
    }

    public void initializeList() {
        Stock obj1 = new Stock(1, "coca", 30.00, 5);
        Stock obj2 = new Stock(2, "String", 40.00, 2);
        Stock obj3 = new Stock(3, "fanta", 50.00, 3);
        Stock obj4 = new Stock(4, "fanta", 50.00, 3);
        Stock obj5 = new Stock(5, "fanta", 50.00, 3);
        Stock obj6 = new Stock(6, "fanta", 50.00, 3);
        Stock obj7 = new Stock(7, "fanta", 50.00, 3);
        object.add(obj1);
        object.add(obj2);
        object.add(obj3);
        object.add(obj4);
        object.add(obj5);
        object.add(obj6);
        object.add(obj7);
        Writefile.Writefile(file, object);
    }

    static int pageSize(int record) {
        int nRecord = object.size();
        int pageSize;
        if ((nRecord % record) == 0) {
            pageSize = nRecord / record;
        } else {
            pageSize = (nRecord / record) + 1;
        }
        return pageSize;
    }

    private void pagination() {
        Scanner scanner = new Scanner(System.in);
        int currentpage;
        int setrow = 3;
        currentpage = 1;

        int choose;
        System.out.print("Page " + currentpage + "/" + pageSize(setrow) + "");

        do {
            if (object.size() > currentpage * setrow) {
                for (int i = (currentpage * setrow - setrow);
                     i < currentpage * setrow; i++) {
                    System.out.println(object.get(i));
                }
            } else {
                for (int i = (currentpage * setrow - setrow); i < object.size(); i++) {
                    System.out.println(object.get(i));
                }
            }

            System.out.println("\nPagination");
            System.out.println("" +
                    "1. Next \n" +
                    "2. Previous \n" +
                    "3. First page \n" +
                    "4. Last page \n" +
                    "5. Go to \n" +
                    "0. Exit!");
            System.out.println("-------------------------------------");
            System.out.print("-> Choose option (1 -5) : ");
            choose = scanner.nextInt();
            if (choose < 0 || choose > 5) {
                System.out.println("Choose Page : ");
            }

            switch (choose) {
                case 1:
                    if (currentpage < setrow)
                        currentpage++;
                    System.out.println("Page " + currentpage + "/" + pageSize(setrow) + "");
                    break;
                case 2:
                    if (currentpage >= 1)
                        currentpage--;
                    System.out.println("Page " + currentpage + "/" + pageSize(setrow) + "");
                    break;
                case 3:
                    currentpage = 1;
                    System.out.println("Page " + currentpage + "/" + pageSize(setrow) + "");
                    break;
                case 4:
                    currentpage = setrow;
                    System.out.println("Page " + currentpage + "/" + pageSize(setrow) + "");
                    break;
                case 5:
                    currentpage = scanner.nextInt();
                    System.out.println("Page " + currentpage + "/" + pageSize(setrow) + "");
                    break;
                case 0:
                    System.out.println("-> Good bye!");
                    System.exit(0);
            }

        } while (choose != 0);
    }

}
