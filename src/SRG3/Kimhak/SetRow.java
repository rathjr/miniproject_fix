package SRG3.Kimhak;

import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.List;
import java.util.Scanner;

public class SetRow {
    public static void SetRow(List<Stock> object){
        Scanner sc = new Scanner(System.in);
        int count=0,chunk=0;
        do {
            try {
                System.out.print("Please enter row for display: ");
                chunk = sc.nextInt();
                count = 0;
                if (chunk < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            sc.nextLine();
        } while (count != 0);
        int totalPage;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }

        int page = 1;
        int length;

        int tmpRow = (page * chunk) - chunk;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID", numberStyle);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Qty", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());

    }
}
