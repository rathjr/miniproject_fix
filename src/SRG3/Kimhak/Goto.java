package SRG3.Kimhak;

import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Goto {
    public static void Goto(List<Stock> object, int page) {
        Scanner sc = new Scanner(System.in);
        int chunk = 3;
        int totalPage;
        if (object.size() % chunk != 0) {
            totalPage = (object.size() / chunk) + 1;
        } else {
            totalPage = (object.size() / chunk);
        }
        int length;

        int tmpRow = (page * chunk) - chunk;
        if (page != totalPage) {
            length = page * chunk;
        } else {
            length = object.size();
        }

        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID", numberStyle);
        t.addCell("Name", numberStyle);
        t.addCell("Unit Price", numberStyle);
        t.addCell("Qty", numberStyle);
        t.addCell("Imported Date", numberStyle);
        for (int row = tmpRow; row < length; row++) {

            t.addCell("" + object.get(row).getId(), numberStyle);
            t.addCell("" + object.get(row).getName(), numberStyle);
            t.addCell("" + object.get(row).getUnitprice(), numberStyle);
            t.addCell("" + object.get(row).getQty(), numberStyle);
            t.addCell("" + object.get(row).getMyObj(), numberStyle);

        }
        Table t1 = new Table(2, BorderStyle.CLASSIC_LIGHT, ShownBorders.SURROUND);
        CellStyle right = new CellStyle(CellStyle.HorizontalAlign.right);
        CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
        t1.setColumnWidth(0, 60, 100);
        t1.addCell(" Page: " + page + "/" + totalPage,left);
        t1.addCell("Total records: " + object.size()+" ",right);
        System.out.println(t.render());
        System.out.println(t1.render());

//        Stock obj1=new Stock(1,"coca",30.00,5);
//        Stock obj2=new Stock(2,"String",40.00,2);
//        Stock obj3=new Stock(3,"fanta",50.00,3);
//        Stock obj4=new Stock(4,"coca",30.00,5);
//        Stock obj5=new Stock(5,"String",40.00,2);
//        Stock obj6=new Stock(6,"fanta",50.00,3);
//        Stock obj7=new Stock(7,"coca",30.00,5);
//        Stock obj8=new Stock(8,"String",40.00,2);
//        Stock obj9=new Stock(9,"fanta",50.00,3);
//        Stock obj10=new Stock(10,"coca",30.00,5);
//        Stock obj11=new Stock(11,"String",40.00,2);
//        Stock obj12=new Stock(12,"fanta",50.00,3);
//        object.add(obj1);
//        object.add(obj2);
//        object.add(obj3);
//        object.add(obj4);
//        object.add(obj5);
//        object.add(obj6);
//        object.add(obj7);
//        object.add(obj8);
//        object.add(obj9);
//        object.add(obj10);
//        object.add(obj11);
//        object.add(obj12);
//        int chunk=3;
//        int totalPage;
//        if(object.size()%chunk != 0){
//            totalPage= (object.size()/chunk)+1;
//        }else{
//            totalPage= (object.size()/chunk);
//        }
//        int page = 1;
//        int choose = 0;
//        int length;
//
//        do {
//            int tmpRow = (page * chunk) - chunk;
//            if (page != totalPage) {
//                length = page * chunk;
//            } else {
//                length = object.size();
//            }
//
//            CellStyle center = new CellStyle(CellStyle.HorizontalAlign.center);
//            CellStyle left = new CellStyle(CellStyle.HorizontalAlign.left);
//            Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
//            t.addCell("ID", center);
//            t.addCell("Name", center);
//            t.addCell("Unit Price", center);
//            t.addCell("Qty", center);
//            t.addCell("Imported Date", center);
//            if (object.size() > 0) {
//                for (int row=tmpRow; row < length; row++) {
//                    if (object.get(row).getId() != 0) {
//                        t.addCell(String.valueOf(object.get(row).getId()), left);
//                        t.addCell(object.get(row).getName(), left);
//                        t.addCell(String.valueOf(object.get(row).getUnitprice()), left);
//                        t.addCell(String.valueOf(object.get(row).getQty()), left);
//                        t.addCell(String.valueOf(object.get(row).getMyObj()), left);
//                    }
//                }
//                System.out.println(t.render());
//            }
//            CellStyle right = new CellStyle(CellStyle.HorizontalAlign.center);
//            Table t1 = new Table(5, BorderStyle.CLASSIC_LIGHT_WIDE, ShownBorders.ALL);
//            t1.addCell("Page: "+page+"/"+totalPage,left);
//            t1.addCell("Total record: "+object.size(),right);
//            System.out.println(t1.render());
//
//            System.out.println("Option of Pagination: "+"\n1.Next\n2.Previous\n3.First page\n4.Last page\n" +
//                    "5.Goto\n0.Exit");
//            System.out.println("Choose option: ");
//            choose = sc.nextInt();
//            switch (choose){
//                case 1:
//                    if(page<totalPage){
//                        page++;
//                    }
//                    break;
//                case 2:
//                    if(page<totalPage){
//                        --page;
//                    }
//                    break;
//                case 3:
//                    page =1;
//                    break;
//                case 4:
//                    page = totalPage;
//                    break;
//                case 5:
//                    System.out.println("Go to page: ");
//                    page = sc.nextInt();
//                    break;
//            }
//
//        }while (choose != 0);
//
//    }
//    public static void main(String[] args){
//        List<Stock> obj=new ArrayList<>() ;
//        Goto(obj);
//    }
    }
}

