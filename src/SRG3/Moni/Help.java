package SRG3.Moni;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class Help {
    public static void help () {
        Table t = new Table(1, BorderStyle.CLASSIC_COMPATIBLE_WIDE, ShownBorders.SURROUND);
        t.setColumnWidth(0, 60, 150);
        t.addCell("1.    press      *: Display all record of products");
        t.addCell("2.    press      W: Add new product");
        t.addCell("3.    press      #w-{Name}-{Price}-{Qty} : Shorthand for Add new product");
        t.addCell("4.    press      R: Read Content any content");
        t.addCell("5.    press      $r-{ID}:  Shorthand for ReadByid");
        t.addCell("6.    press      U: Update data");
        t.addCell("7.    press      D: Delete data");
        t.addCell("8.    press      @d-{ID}: shorthand for Delete data");
        t.addCell("9.    press      F: Display First Page");
        t.addCell("10.    press      P: Display Previous Page");
        t.addCell("11.    press      N: Display Next Page");
        t.addCell("12.    press      L: Display Last Page");
        t.addCell("13.   press      S: Search product by name");
        t.addCell("14.   press      1k: for add 1k data");
        t.addCell("15.   press      1M: for add 1M data");
        t.addCell("16.   press      10M: for add 10M data");
        t.addCell("17.   press     Sa: Save record to file");
        t.addCell("18.   press     Ba: Backup data");
        t.addCell("19.   press     Re: Restore data");
        t.addCell("20.   press      H: Help");
        System.out.println(t.render());
    }



}
