package SRG3.Moni;


import SRG3.Stock;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.List;

public class TableDisplay {
    public void ShowList(List<Stock> object){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL);

        t.setColumnWidth(0, 8, 16);
        t.setColumnWidth(1, 16, 32);
        t.setColumnWidth(2, 16, 32);
        t.setColumnWidth(3, 16, 32);
        t.setColumnWidth(4, 16, 32);

        t.addCell("ID",numberStyle);
        t.addCell("Name",numberStyle);
        t.addCell("Unit Price",numberStyle);
        t.addCell("Qty",numberStyle);
        t.addCell("Imported Date",numberStyle);
        for(Stock s:object){

            t.addCell(""+s.getId(),numberStyle);
            t.addCell(s.getName(), numberStyle);
            t.addCell(""+s.getUnitprice(), numberStyle);
            t.addCell(""+s.getQty(),numberStyle);
            t.addCell(""+s.getMyObj(),numberStyle);

        }
        System.out.println(t.render());




    }



}
